# Vagrant with ansible

Ansible n'a pas besoin de client sur toutes les machines, il suffit d'avoir python (python toujours dl sur des distrib linux rare ou il y a pas par defaut)

`ansible -i config/ service_web -m ping`

permet de verifier la connexion ssh au groupe de serveur `service_web` en precisant l'inventaire `-i`

### private key

password : ansible

### CLI

`ansible --list-host all`

# Mise en place de l'infrastructure pour Ansible

## Générer une paire de clefs SSH dans le meme dossier avec

    ssh-keygen -f ansible_rsa

## Re-provisionner si besoin

Avec

    vagrant rsync && vagrant provision

Pour démarrer SSH-AGENT si besoin ⁼^.^=

    eval $(ssh-agent -s)

Pour charger la clef en mémoire

    ssh-add ~/.ssh/ansible_rsa

## Pour valider automatiquement les clefs

Ajouter la liste des machines dans le fichier liste-pour-ssh

    cat liste-pour-ssh \
      |xargs -iSERVER ssh-keyscan SERVER  >> ~/.ssh/known_hosts
