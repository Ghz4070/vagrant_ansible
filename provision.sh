#!/bin/sh

set -ue

# Je récupere le hostname de la VM
HOSTNAME="$(hostname)"

## Verifier que la clef pour ANSIBLE est presente avant de continuer
if [ ! -f /vagrant/.ssh/client/id_rsa_vagrant_ansible ]; then
    echo >&2 "ERROR: unable to find /vagrant/id_rsa_vagrant_ansible keyfile"
    exit 1
fi
if [ ! -f /vagrant/.ssh/server/id_rsa_vagrant_ansible.pub ]; then
    echo >&2 "ERROR: unable to find /vagrant/id_rsa_vagrant_ansible.pub keyfile"
    exit 1
fi

apt-get update
apt-get install -y \
    ca-certificates \
    curl \
    wget \
    vim

# Si la machine s'appelle control
if [ "$HOSTNAME" = "serverControl" ]; then
    # J'installe ansible dessus
    apt-get install -y \
        ansible

    mkdir -p /root/.ssh
    cp /vagrant/.ssh/client/id_rsa_vagrant_ansible /home/vagrant/.ssh
    chmod 0600 /home/vagrant/.ssh/id_rsa_vagrant_ansible
    chown -R vagrant:vagrant /home/vagrant/.ssh

    # Utilisation du SSH-AGENT pour charger les clés une fois pour toute
    # et ne pas avoir à retaper les password des clefs
    sed -i \
        -e '/## BEGIN PROVISION/,/## END PROVISION/d' \
        /home/vagrant/.bashrc
    cat >>/home/vagrant/.bashrc <<-MARK
	## BEGIN PROVISION
	eval \$(ssh-agent -s)
	ssh-add ~/.ssh/id_rsa_vagrant_ansible
	## END PROVISION
	MARK
fi

# J'utilise /etc/hosts pour associer les IP aux noms de domaines
# sur mon réseau local, sur chacune des machines
sed -i \
    -e '/^## BEGIN PROVISION/,/^## END PROVISION/d' \
    /etc/hosts
cat >>/etc/hosts <<MARK
## BEGIN PROVISION
192.168.60.3    serverControl
192.168.60.4    serverOne
192.168.60.5    serverTwo
192.168.60.6    databaseOne
## END PROVISION
MARK

# J'autorise la clef sur tous les serveurs
mkdir -p /root/.ssh
cat /vagrant/.ssh/server/id_rsa_vagrant_ansible.pub >>/root/.ssh/authorized_keys

# Je vire les duplicata (potentiellement gênant pour SSH)
sort -u /root/.ssh/authorized_keys >/root/.ssh/authorized_keys.tmp
mv /root/.ssh/authorized_keys.tmp /root/.ssh/authorized_keys

# Je corrige les permissions
touch /root/.ssh/config
chmod 0600 /root/.ssh/*
chmod 0644 /root/.ssh/config
chmod 0700 /root/.ssh
chown -R vagrant:vagrant /home/vagrant/.ssh

# mkdir -p /home/vagrant/.ssh
# cat /vagrant/.ssh/server/id_rsa_vagrant_ansible.pub >>/home/vagrant/.ssh/authorized_keys
# chmod 0700 /home/vagrant/.ssh
# chmod 0600 /home/vagrant/.ssh/authorized_keys
# chown -R vagrant:vagrant /home/vagrant/.ssh

echo "SUCCESS"
